﻿
namespace App.ViewModels
{
    using Helpers;
    using Models;
    using System.Collections.ObjectModel;
    public class MainViewModel : BaseViewModel
    {

        //#region Attibrutes
        //private UserLocal user;
        //#endregion

        #region ViewModels

        public ChangePasswordViewModel ChangePassword
        {
            get;
            set;
        }
        public MyProfileViewModel MyProfile
        {
            get;
            set;
        }
        public TokenResponse Token
        {
            get;
            set;
        }
        //public UserLocal User
        //{
        //    get { return this.user; }
        //    set { SetValue(ref this.user, value); }
        //}

 

        public RegisterViewModel Register
        {
            get;
            set;
        }


        #endregion

        #region Properties
        public ObservableCollection<MenuItemViewModel> Menus
        {
            get;
            set;
        }
        public string SelectedModule
        {
            get;
            set;
        }
        #endregion

        #region ViewModels


    
        public LoginViewModel Login
        {
            get;
            set;
        }

        public EmployeesViewModel Employee
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            instance = this;
            LoadMenu();
            Login = new LoginViewModel();
            Employee = new EmployeesViewModel();

        }
        #endregion

        #region Singleton
        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion
        #region Methods
        private void LoadMenu()
        {
            this.Menus = new ObservableCollection<MenuItemViewModel>();
           
            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ic_settings",
                PageName = "MyProfilePage",
                Title = Languages.MyProfile,
            });



            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ic_exit_to_app",
                PageName = "LoginPage",
                Title = Languages.LogOut,
            });
        }
        #endregion
    }
}
