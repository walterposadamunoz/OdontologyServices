﻿using App.Models;
using App.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace App.ViewModels
{
    public class EmployeesViewModel : BaseViewModel
    {
        
        #region Singleton
        private static EmployeesViewModel instance;

        public static EmployeesViewModel GetInstance()
        {

            if (instance == null)
            {
                instance = new EmployeesViewModel();
            }

            return instance;
        }
        #endregion

        #region Services
        private ApiService apiService;


        #endregion




        #region Attributes
        private ObservableCollection<Models.Employee> employees;
        private bool isRefreshing;
        private string filter;
        #endregion

        #region Properties
        public ObservableCollection<Models.Employee> Employees
        {
            get { return this.employees; }
            set { SetValue(ref this.employees, value); }
        }

        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value); }
        }

        
        #endregion
        #region Constructor

        public EmployeesViewModel()
        {
            instance = this;
            Employees = new ObservableCollection<Models.Employee>();
            apiService = new ApiService();
            //dialogService = new DialogService();
            //navigationService = new NavigationService();
            //dataService = new DataService();
            LoadAgendas();
        }

        #endregion 
        #region Methods

        public void Update(Models.Employee employee)
        {


            IsRefreshing = true;
            LoadAgendas();
            IsRefreshing = false;

        }
        public void Add(Models.Employee employee)
        {
            IsRefreshing = true;
            employees.Add(employee);
            Employees = new ObservableCollection<Models.Employee>(
                employees.OrderBy(c => c.FullName));
            IsRefreshing = false;
        }
        public async Task Delete(Models.Employee employee)
        {
            IsRefreshing = true;

            var connection = await apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                IsRefreshing = false;
                
                return;
            }

            var mainViewModel = MainViewModel.GetInstance();
            //var parameters = dataService.First<Parameter>(false);
            //var user = dataService.First<User>(true);
            
            var apiSecurity = Application.Current.Resources["APISecurity"].ToString();
            var response = await apiService.Delete(
                apiSecurity,
                "/api/",
                "Employees","","","");
                //user.TokenType,
                //user.AccessToken,
                //agenda);

            if (!response.IsSuccess)
            {
                IsRefreshing = false;
                //await dialogService.ShowMessage(
                //    "Error",
                //    response.Message);
                return;
            }

            employees.Remove(employee);
            Employees = new ObservableCollection<Models.Employee>(
                employees.OrderBy(c => c.FullName));

            IsRefreshing = false;
        }

        public async void LoadAgendas()
        {

            this.IsRefreshing = true;
            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "Accept");
                return;
            }
            var apiSecurity = Application.Current.Resources["APISecurity"].ToString();
            var response = await this.apiService.Get<Employee>(
              apiSecurity,
              "/api",
              "/Employees");

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                return;
            }
            List<Models.Employee> _agendas;
            _agendas = (List<Employee>)response.Result;
            
            Employees = new ObservableCollection<Models.Employee>(
                   _agendas.OrderBy(c => c.FullName));

            this.IsRefreshing = false;



        }

       

        #endregion

        #region Command
        public ICommand RefreshCommand
        {
            get { return new RelayCommand(Refresh); }
        }

        private void Refresh()
        {
            LoadAgendas();
        }

        #endregion
    }
}