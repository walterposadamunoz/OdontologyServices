﻿using App.VIews;


using Xamarin.Forms;

namespace App
{
    public partial class App : Application
    {
        #region Properties
        public static NavigationPage Navigator { get; internal set; }

        public static MasterPage Master { get; internal set; }
        #endregion
        public App()
        {
            InitializeComponent();

            MainPage = new NewUserPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
