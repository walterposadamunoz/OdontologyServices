﻿using App.Helpers;
using App.Models;
using App.Services;
using App.ViewModels;
using App.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App
{
    public partial class App : Application
    {
        #region Properties

        public static NavigationPage Navigator { get; internal set; }
        public static MasterPage Master { get; internal set; }
        private DataService dataService;
        #endregion

        public App()
        {
            InitializeComponent();
            dataService = new DataService();
            LoadParameters();


            if (Settings.IsRemembered == "true")
            {

                var token = dataService.First<TokenResponse>(false);
                //dataService.Insert<Parameter>(new Parameter { Option="None"});
                if (token != null && token.Expires > DateTime.Now)
                {
                    var user = dataService.First<UserLocal>(false);
                    var mainViewModel = MainViewModel.GetInstance();
                    mainViewModel.Token = token;
                    mainViewModel.User = user;

                    Application.Current.MainPage = new MasterPage();
                }
                else
                {
                    this.MainPage = new LoginPage();
                }
            }
            else
            {
                this.MainPage = new LoginPage();
            }
        }
        public static async Task NavigateToProfile(Models.FacebookResponse profile)
        {
            if (profile == null)
            {
                Application.Current.MainPage = (new LoginPage());
                return;
            }

            var apiService = new APIService();
            var dataService = new DataService();

            var apiSecurity = Application.Current.Resources["APISecurity"].ToString();
            var token = await apiService.LoginFacebook(
                apiSecurity,
                "/api",
                "/Users/LoginFacebook",
                profile);

            if (token == null)
            {
                Application.Current.MainPage = new LoginPage();
                return;
            }

            var user = await apiService.GetUserByEmail(
                apiSecurity,
                "/api",
                "/Users/GetUserByEmail",
                token.TokenType,
                token.AccessToken,
                token.UserName);

            UserLocal userLocal = null;
            userLocal = (UserLocal)user.Result;
            if (user != null)
            {
                
                dataService.DeleteAllAndInsert(user);
            }

            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Token = token;
            mainViewModel.User = userLocal;
            
            Application.Current.MainPage = new MasterPage();
            Settings.IsRemembered = "true";

            
        }
        public static Action HideLoginView
        {
            get
            {
                return new Action(() => Application.Current.MainPage =
                                  new NavigationPage(new LoginPage()));
            }
        }

        private void LoadParameters()
        {

            var parameter = dataService.First<Parameter>(false);
            if (parameter == null)
            {
                parameter = new Parameter
                {
                    Option = "None"
                };

                dataService.Insert(parameter);
            }
            else
            {
                dataService.Update(parameter);
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
