﻿

namespace App.Models
{
    using SQLite.Net.Attributes;
    using SQLiteNetExtensions.Attributes;
    using System;

    public class UserLocal
    {
        [PrimaryKey]
        public int UserId { get; set; }

        public string NickName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Picture { get; set; }

        public string Password { get; set; }

        



        public byte[] ImageArray { get; internal set; }

        public string AccessToken { get; set; }

        public string TokenType { get; set; }

        public DateTime TokenExpires { get; set; }

        public bool IsRemembered { get; set; }

        public int UserTypeId { get; set; }
        [ManyToOne]
        public UserType UserType { get; set; }


        public string ImageFullPath
        {
            get
            {
                if (string.IsNullOrEmpty(Picture))
                {
                    return string.Format("no_image.png");
                }

                if (this.UserTypeId == 1)
                {
                    return string.Format(
                        "https://destintsapi.azurewebsites.net{0}",
                        Picture.Substring(1));

                }
                return Picture;
            }
        }

        public override int GetHashCode()
        {
            return UserId;
        }

    }
}

