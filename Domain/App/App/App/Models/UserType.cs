﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Models
{
    public class UserType
    {
        [PrimaryKey]
        public int UserTypeId { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        public List<UserLocal> Users { get; set; }

        public string Name { get; set; }



        public override int GetHashCode()
        {
            return UserTypeId;
        }
    }
}