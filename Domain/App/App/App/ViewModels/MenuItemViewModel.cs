﻿

namespace App.ViewModels
{
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Views;
    using Xamarin.Forms;
    using Helpers;
    using Services;
    using Models;

    public class MenuItemViewModel
    {
        private DataService dataService;

        public MenuItemViewModel()
        {
            dataService = new DataService();
        }
        #region Properties
        public string Icon { get; set; }

        public string Title { get; set; }

        public string PageName { get; set; }
        #endregion

        #region Commands
        public ICommand NavigateCommand
        {
            get
            {
                return new RelayCommand(Navigate);
            }
        }

        private void Navigate()
        {
            App.Master.IsPresented = false;

            if (this.PageName == "LoginPage")
            {
                Settings.IsRemembered = "false";
                var mainViewModel = MainViewModel.GetInstance();
                mainViewModel.Token = null;
                mainViewModel.User = null;
                Application.Current.MainPage = new NavigationPage(
                    new LoginPage());
            }
            else if (this.PageName == "MyProfilePage")
            {
                MainViewModel.GetInstance().MyProfile = new MyProfileViewModel();
                App.Navigator.PushAsync(new MyProfilePage());
            }
            else if (this.PageName == "MyAgendaPage")
            { 
                MainViewModel.GetInstance().MyAgenda = new MyAgendaViewModel(MainViewModel.GetInstance().User.UserId);
                App.Navigator.PushAsync(new MyAgendaPage());
            }

        
        }
        #endregion
    }
}