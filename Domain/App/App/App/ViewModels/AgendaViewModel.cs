﻿using App.Models;
using App.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App.ViewModels
{
    public class AgendaViewModel:BaseViewModel
    {
        #region Services
        private APIService apiService;
        private DataService dataService;
        private NavigationService navigationService;

        #endregion
        #region Attributes
        private ObservableCollection<Models.Agenda> agendas;
        private bool isRefreshing;

        #endregion

        #region Properties
        public ObservableCollection<Models.Agenda> Agendas
        {
            get { return this.agendas; }
            set { SetValue(ref this.agendas, value); }
        }

        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value); }
        }


        #endregion
        public AgendaViewModel()
        {
            Agendas = new ObservableCollection<Models.Agenda>();
            apiService = new APIService();

            navigationService = new NavigationService();
            dataService = new DataService();
        }
        #region Singleton
        private static AgendaViewModel instance;

        public static AgendaViewModel GetInstance()
        {
            if (instance == null)
            {
                return new AgendaViewModel();
            }

            return instance;
        }
        #endregion

        public AgendaViewModel(int id)
        {
            instance = this;
            Agendas = new ObservableCollection<Models.Agenda>();
            apiService = new APIService();

            navigationService = new NavigationService();
            dataService = new DataService();
            LoadAgendas(id);
        }
        #region Methods

        public void Update(Models.Agenda employee)
        {


            IsRefreshing = true;
            LoadAgendas(0);
            IsRefreshing = false;

        }
        public void Add(Models.Agenda agenda)
        {
            IsRefreshing = true;
            agendas.Add(agenda);
            Agendas = new ObservableCollection<Models.Agenda>(
                agendas.OrderBy(c => c.Description));
            IsRefreshing = false;
        }
        public async Task Delete(Models.Agenda agenda)
        {
            IsRefreshing = true;

            var connection = await apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                IsRefreshing = false;

                return;
            }

            var mainViewModel = MainViewModel.GetInstance();
            var parameters = dataService.First<Parameter>(false);
            var user = dataService.First<UserLocal>(true);

            var apiSecurity = Application.Current.Resources["APISecurity"].ToString();
            var response = await apiService.Delete(
                apiSecurity,
                "/api/",
                "Agenda",
               MainViewModel.GetInstance().Token.TokenType,
                MainViewModel.GetInstance().Token.AccessToken,
                agenda);

            if (!response.IsSuccess)
            {
                IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                                    "Error",
                                    connection.Message,
                                    "Accept");
                return;
            }

            
            AgendaViewModel.GetInstance().Add(agenda);
            

            IsRefreshing = false;
        }

        public async void LoadAgendas(int UserId)
        {

            this.IsRefreshing = true;
            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "Accept");
                return;
            }
            var apiSecurity = Application.Current.Resources["APISecurity"].ToString();
            var response = await this.apiService.Get<Agenda>(
              apiSecurity,
              "/api/Agenda/",UserId.ToString()
              );

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                return;
            }
            List<Models.Agenda> _agendas;
            _agendas = (List<Agenda>)response.Result;
            Agendas = new ObservableCollection<Models.Agenda>(
          _agendas);
            var mainViewModel = MainViewModel.GetInstance();
           
            for (int i = 420; i < 1080; i=i+60)
            {
               
                  
                        Agendas.Add(
                            new Agenda {
                                Date= DateTime.Now,
                                Description="Disponible",
                                Duration="30m",
                                Hour = TimeSpan.FromMinutes(i),
                                EmployeeId=UserId,
                                UserId= mainViewModel.User.UserId,
                                
                            }
                           
                            );
                     
                   
                
            }
            Agendas.OrderByDescending(c => c.Hour);

            this.IsRefreshing = false;



        }



        #endregion
    }
}
