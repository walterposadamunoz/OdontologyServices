﻿
using App.Models;
using App.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App.ViewModels
{
    public class DetailEmployeeViewModel:BaseViewModel
    {
       
      
        
        #region  Properties

        public long EmployeeId { get; set; }

      
        public string FirstName { get; set; }

       
        public string LastName { get; set; }

       
        public string Picture { get; set; }

        
        public string Email { get; set; }

   
        public string Phone { get; set; }

        public string NickName { get; set; }

        
        public string FullName { get; set; }

        public string FullPicture { get; set; }



        #endregion
        public Employee Employee{ get; set; }
        
        
        public DetailEmployeeViewModel(Employee employee)
        {
            this.Employee = employee;
            Phone = Employee.Phone;
            FirstName = Employee.FirstName;
            LastName = Employee.LastName;
            Email = Employee.Email;
            
            Picture = Employee.Picture;
            FullPicture=Employee.FullPicture;
            FullName = Employee.FullName;
            
            
        }
        
    }
}
