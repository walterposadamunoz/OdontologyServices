﻿using App.Helpers;
using App.Models;
using App.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace App.ViewModels
{
    public class MainViewModel : BaseViewModel
    {

        #region Attibrutes
        private UserLocal user;
        NavigationService navigationService;
        #endregion

        #region ViewModels
        public MyAgendaViewModel MyAgenda
        {
            get;
            set;
        }
        public DetailEmployeeViewModel DetailEmployee
        {
            get;
            set;
        }
        public ChangePasswordViewModel ChangePassword
        {
            get;
            set;
        }
        public EditViewModel Edit
        {
            get;
            set;
        }
        public DetailsViewModel Details
        {
            get;
            set;
        }
        

        public MyProfileViewModel MyProfile
        {
            get;
            set;
        }
        public EmployeesViewModel Employee
        {
            get;
            set;
        }
        public AgendaViewModel Agenda
        {
            get;
            set;
        }
        
        public TokenResponse Token
        {
            get;
            set;
        }
        public UserLocal User
        {
            get { return this.user; }
            set { SetValue(ref this.user, value); }
        }

        

        public RegisterViewModel Register
        {
            get;
            set;
        }


        #endregion

        #region Properties
        public ObservableCollection<MenuItemViewModel> Menus
        {
            get;
            set;
        }
        public string SelectedModule
        {
            get;
            set;
        }

        public LoginViewModel Login
        {
            get;
            set;
        }
        #endregion

      

        #region Constructors
        public MainViewModel()
        {
            instance = this;
            
            Register = new RegisterViewModel();
            Employee = new EmployeesViewModel();
            navigationService = new NavigationService();
            LoadMenu();
            Login = new LoginViewModel();
            
        }
        #endregion

        #region Singleton
        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion
        #region Methods
      

        private void LoadMenu()
        {
            this.Menus = new ObservableCollection<MenuItemViewModel>();

            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ic_agenda.png",
                PageName = "MyAgendaPage",
                Title = Languages.Agenda,
            });
            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ic_profile",
                PageName = "MyProfilePage",
                Title = Languages.MyProfile,
            });



            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ic_exit.png",
                PageName = "LoginPage",
                Title = Languages.LogOut,
            });
        }
        #endregion
    }
}
