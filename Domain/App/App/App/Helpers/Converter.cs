﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Helpers
{
    using System;

    using Models;

    public static class Converter
    {
        public static UserLocal ToUserLocal(UserLocal user)
        {
            return new UserLocal
            {
                Email = user.Email,
                FirstName = user.FirstName,
                Picture = user.Picture,
                LastName = user.LastName,
                
                UserId = user.UserId,
                UserTypeId = user.UserTypeId,
            };
        }

        public static UserLocal ToUserDomain(UserLocal user, byte[] imageArray)
        {
            return new UserLocal
            {
                Email = user.Email,
                FirstName = user.FirstName,
                Picture = user.Picture,
                LastName = user.LastName,
                
                UserId = user.UserId,
                UserTypeId = user.UserTypeId,
                ImageArray = imageArray,
                NickName= user.NickName
                
            };
        }
    }
}