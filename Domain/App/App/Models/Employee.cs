﻿
namespace App.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Xamarin.Forms;
    using Helpers;

    public partial class Employee
    {
        #region  Properties
        [JsonProperty("EmployeeId")]
        public long EmployeeId { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("Picture")]
        public string Picture { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("NickName")]
        public string NickName { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        public string FullPicture
        {
            get
            {
                if (string.IsNullOrEmpty(Picture))
                {
                    return "avatar_user.png";
                }

                else
                {
                    return string.Format("http://servicesbackendvet.azurewebsites.net{0}", Picture.Substring(1));
                }




            }
        }

        #endregion
        #region Commands
    
       

        public ICommand SelectEmployeeCommand
        {
            get
            {
                return new RelayCommand(SelectEmployee);
            }
        }

        private async void SelectEmployee()
        {
            await Application.Current.MainPage.DisplayAlert(
                      Languages.Error,
                      Languages.LoginError,
                      Languages.Accept);
        }

       
        #endregion
    }

}
