﻿using Backend.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Backend
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Models.DataContextLocal, Migrations.Configuration>());
            CreateRolesAndSuperuser();
        }
        private void CreateRolesAndSuperuser()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            CreateRoles(db);
            CreateSuperuser(db);
            AddPermisionsToSuperuser(db);
            db.Dispose();
        }

        private void CreateRoles(ApplicationDbContext db)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            if (!roleManager.RoleExists("Admin"))
            {
                var result = roleManager.Create(new IdentityRole("Admin"));
            }

            if (!roleManager.RoleExists("User"))
            {
                var result = roleManager.Create(new IdentityRole("User"));
            }

            if (!roleManager.RoleExists("Dentist"))
            {
                var result = roleManager.Create(new IdentityRole("Dentist"));
            }


        }

        private void CreateSuperuser(ApplicationDbContext db)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var user = userManager.FindByName("walterposadamunoz@gmail.com");
            if (user == null)
            {
                user = new ApplicationUser();
                user.UserName = "walterposadamunoz@gmail.com";
                user.Email = "walterposadamunoz@gmail.com";
                var result = userManager.Create(user, "123456");
            }
            var use2r = userManager.FindByName("Yohanlopez18@gmail.com");
            if (use2r == null)
            {
                use2r = new ApplicationUser();
                use2r.UserName = "Yohanlopez18@gmail.com";
                use2r.Email = "Yohanlopez18@gmail.com";
                var result = userManager.Create(use2r, "yohanlopez18");
            }
        }

        private void AddPermisionsToSuperuser(ApplicationDbContext db)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var user = userManager.FindByName("walterposadamunoz@gmail.com");

            if (!userManager.IsInRole(user.Id, "Admin"))
            {
                userManager.AddToRole(user.Id, "Admin");
            }

            if (!userManager.IsInRole(user.Id, "User"))
            {
                userManager.AddToRole(user.Id, "User");
            }

            if (!userManager.IsInRole(user.Id, "Dentist"))
            {
                userManager.AddToRole(user.Id, "Dentist");
            }

            var user2 = userManager.FindByName("Yohanlopez18@gmail.com");

            if (!userManager.IsInRole(user2.Id, "Admin"))
            {
                userManager.AddToRole(user2.Id, "Admin");
            }

            if (!userManager.IsInRole(user2.Id, "User"))
            {
                userManager.AddToRole(user2.Id, "User");
            }

            if (!userManager.IsInRole(user2.Id, "Dentist"))
            {
                userManager.AddToRole(user2.Id, "Dentist");
            }


        }


    }
}
