﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using Domain;
using Newtonsoft.Json.Linq;

namespace API.Controllers
{
    [RoutePrefix("api/Agendas")]
    public class AgendaController : ApiController
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: api/Agenda
        public IQueryable<Agenda> GetAgendas()
        {
            return db.Agendas;
        }

        // GET: api/Agenda/5
        [ResponseType(typeof(Agenda))]
        public async Task<IHttpActionResult> GetAgenda(int id)
        {
            Agenda agenda = await db.Agendas.FindAsync(id);
            if (agenda == null)
            {
                return NotFound();
            }

            return Ok(agenda);
        }
        [HttpPost]
        [Route("GetUserByEmail")]
        public async Task<IHttpActionResult> GetAgendaByUser(JObject form)
        {
            var userid = string.Empty;
            dynamic jsonObject = form;

            try
            {
                userid = jsonObject.UserId.Value;
            }
            catch
            {
                return BadRequest("Incorrect call");
            }

            var user = await db.Agendas
                .Where(u => u.UserId == Convert.ToInt32(userid))
                .FirstOrDefaultAsync();
            if (user == null)
            {
                return NotFound();
            }



            return Ok(user);
        }
        // PUT: api/Agenda/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAgenda(int id, Agenda agenda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != agenda.AgendaId)
            {
                return BadRequest();
            }

            db.Entry(agenda).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AgendaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Agenda
        [ResponseType(typeof(Agenda))]
        public async Task<IHttpActionResult> PostAgenda(Agenda agenda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Agendas.Add(agenda);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = agenda.AgendaId }, agenda);
        }

        // DELETE: api/Agenda/5
        [ResponseType(typeof(Agenda))]
        public async Task<IHttpActionResult> DeleteAgenda(int id)
        {
            Agenda agenda = await db.Agendas.FindAsync(id);
            if (agenda == null)
            {
                return NotFound();
            }

            db.Agendas.Remove(agenda);
            await db.SaveChangesAsync();

            return Ok(agenda);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AgendaExists(int id)
        {
            return db.Agendas.Count(e => e.AgendaId == id) > 0;
        }
    }
}