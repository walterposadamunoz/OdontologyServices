﻿

using System.Data.Entity;

namespace Domain
{

    public class DataContext : DbContext
    {
        public DataContext() : base("DefaultConnection")
        {
        }

        

        public DbSet<Agenda> Agendas { get; set; }
        public DbSet<Employee> Employees { get; set; }

      

        public DbSet<User> Users { get; set; }

        public DbSet<UserType> UserTypes { get; set; }
    }
}
