﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Agenda
    {
        [Key]
        public int AgendaId { get; set; }
        
        public DateTime Date { get; set; }
        
        public TimeSpan Hour { get; set; }


        [Display(Name = "User123 ")]
        public int UserId { get; set; }

        public virtual User User { get; set; }

        [Display(Name = "Employee")]
        public int EmployeeId { get; set; }

        public virtual Employee Employee{ get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Turn Duration")]
        public double DurationTurn{ get; set; }


    }
}
